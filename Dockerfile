FROM centos:7

RUN yum install python3 python3-pip -y

WORKDIR /opt/python-api
COPY ["requirements.txt", "python-api.py", "./"]

RUN pip3 install -r requirements.txt
CMD ["python3", "python-api.py"]
